# Solidity-PoC

This Project was just a PoC to practice solidity.

## How to use it?

Download Truffle and Ganache from: https://www.trufflesuite.com/

Start Ganache on Port 7545 and on Network ID 5777

Navigate to the billing root dir via bash or cmd<br>use the command: truffle --reset --compile-all<br>then use: truffle migrate

Now the contract should be deployed on your ganache. There are different tools to interact with your new contract.<br>You can access it via truffle: truffle console

Learn how to interact with a Solidity-Contract on: https://www.trufflesuite.com/docs/truffle/getting-started/interacting-with-your-contracts
