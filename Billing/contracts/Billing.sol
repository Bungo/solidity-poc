pragma solidity ^0.5.8;

contract Billing{
    address public owner;
    uint billcount;
    uint256 OpenBillThreshold;
    mapping (address => Bill[]) public openBills;
    mapping (address => Bill[]) public payedBills;
    mapping (address => uint) public balances;
        
    constructor() public{
        owner = msg.sender;
        billcount++;
        OpenBillThreshold += 10000;
    }

    struct Bill{
        uint finalPayment;
        uint timestamp;
        address biller;
    }

    event Payed(address from, address to, uint billId);
    event NewBill(address from, uint billId);
    event IncreasedBalance(address receiver, uint amount);
    event DrawMoney(address receiver, uint amount);
    
    function paybill(uint toPayId) public{
        Bill memory toPay = openBills[msg.sender][toPayId];
        require(msg.sender != toPay.biller, "Can't pay yourself!");
        require(openBills[msg.sender][toPayId].finalPayment != 0, "Bill not existing!");
        require(toPay.finalPayment <= balances[msg.sender], "Insufficient balance.");
        balances[msg.sender] -= toPay.finalPayment;
        balances[toPay.biller] += toPay.finalPayment;
        payedBills[msg.sender][toPayId] = openBills[msg.sender][toPayId];
        delete openBills[msg.sender][toPayId];
        emit Payed(msg.sender, toPay.biller, toPayId);
    }
    
    function bill(uint cost, address billReceiver) public{
        Bill memory newBill = Bill(cost, now, msg.sender);
        if (cost > balances[billReceiver]){
            require(checkThreshold(openBills[billReceiver]), "You've reached your limit. Please pay your bills!");
            Bill[] memory bills = openBills[billReceiver];
            bills[billcount++] = newBill;
            emit NewBill(msg.sender, billcount);
        }
        else{
            balances[billReceiver] -= cost;
            balances[msg.sender] += cost;
            Bill[] memory bills = payedBills[billReceiver];
            bills[billcount++] = newBill;
            emit Payed(billReceiver, msg.sender, billcount);
        }
    }

    function getBalance() public view returns(uint){
        return balances[msg.sender];
    }

    function increaseBalance(uint amount) public{
        require(amount > 0, "Please increase more than 0!");
        balances[msg.sender] += amount;
        emit IncreasedBalance(msg.sender, amount);
    }

    function withdrawMoney(uint amount) public{
        require(amount > 0, "Please draw more than 0!");
        require(amount <= balances[msg.sender], "You don't have that much money!");
        balances[msg.sender] -= amount;
        emit DrawMoney(msg.sender, amount);
    }

    function checkThreshold(Bill[] memory bills) internal view returns (bool){
        uint amount = 0;
        for(uint i = 0; i < billcount; i++){
            if(bills[i].finalPayment != 0){
                amount += bills[i].finalPayment;
            }
        }
        return amount < OpenBillThreshold;
    }
}